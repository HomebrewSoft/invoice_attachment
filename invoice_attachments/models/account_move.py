from odoo import _, api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    pdf = fields.Boolean(
        string="PDF",
        compute="_compute_booleans_pdf",
    )
    xml = fields.Boolean(
        string="XML",
        compute="_compute_booleans_xml",
    )

    def _compute_booleans_pdf(self):
        for record in self:
            attachment = self.env["ir.attachment"].search(
                [
                    ("res_id", "=", record.id),
                    ("res_model", "=", "account.move"),
                    ("name", "ilike", ".pdf"),
                ],
                limit=1,
            )
            record.pdf = bool(attachment)

    def _compute_booleans_xml(self):
        for record in self:
            attachment = self.env["ir.attachment"].search(
                [
                    ("res_id", "=", record.id),
                    ("res_model", "=", "account.move"),
                    ("name", "ilike", ".xml"),
                ],
                limit=1,
            )
            record.xml = bool(attachment)