{
    "name": "invoice_attachments",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
    ],
    "data": [
        # views
        "views/account_move.xml",
    ],
}
